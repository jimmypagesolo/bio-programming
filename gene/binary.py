import random
import sys

class Logger:
    def __init__(self, driver=None):
        self.driver = driver

    def __print__(self, message):
        if self.driver is not None:
            return self.driver(message)
        else:
            print(message)

    def info(self, message):
        self.__print__("[INFO] %s" % message)

    def error(self, message):
        self.__print__("[ERR] %s" % message)

    def warning(self, message):
        self.__print__("[WARN] %s" % message)

class Individual:

    def __init__(self):
        self.fitness = 0
        self.genes = list()
        self.genes_length = 5
        self.__genes_randomly()

    def __genes_randomly(self):
        # % 2 just to make sure genes generate 0 or 1
        for i in range(self.genes_length):
            self.genes.append(int(round(random.random() * 100)) % 2)

    def calc_fitness(self):
        self.fitness = 0
        for i in range(5):
            if self.genes[i] == 1:
                self.fitness += 1


class Population:

    def __init__(self):
        self.fittest = 0
        self.individuals = list()
        self.individuals_size = 10

    def init_population(self, size):
        for i in range(self.individuals_size):
            self.individuals.append(Individual())

    # get the fittest individual (find MAX)
    def get_fittest(self):
        max_fit = 0
        max_fit_index = 0

        # try to get max fitness
        for i in range(self.individuals_size):
            if max_fit <= self.individuals[i].fitness:
                max_fit = self.individuals[i].fitness
                max_fit_index = i

        self.fittest = self.individuals[max_fit_index].fitness
        return self.individuals[max_fit_index]

    # get the second most fittest individual
    def get_second_fittest(self):
        max_fit1 = 0
        max_fit2 = 0
        for i in range(self.individuals_size):
            if self.individuals[i].fitness >= self.individuals[max_fit1].fitness:
                max_fit1 = i
                max_fit2 = max_fit1
            elif self.individuals[i].fitness >= self.individuals[max_fit2].fitness:
                max_fit2 = i

        return self.individuals[max_fit2]

    # get the least fittest (find MIN)
    def get_least_fittest_index(self):
        min_fit = sys.maxsize
        min_fit_index = 0
        for i in range(self.individuals_size):
            if min_fit >= self.individuals[i].fitness:
                min_fit = self.individuals[i].fitness
                min_fit_index = i
        return min_fit_index

    # calculate fitness
    def calculate_fitness(self):
        for i in range(self.individuals_size):
            self.individuals[i].calc_fitness()


class GeneticProcessor:
    def __init__(self, logger):
        self.generation_count = 0
        self.population = Population()
        self.fittest = None
        self.second_fittest = None
        self.logger = logger

    def run(self):
        # initialize population
        self.population.init_population(10)

        # calculate fitness for each individual
        self.population.calculate_fitness()

        self.logger.info("-> generation: {}. fittest: {}".format(self.generation_count, self.population.fittest))

        self.find_max_fittest_and_add()

        self.logger.info("Solution found at: %s" % self.generation_count)
        self.logger.info("The fitness: %s" % self.population.get_fittest().fitness)

        genes = []
        for i in range(5):
            genes.append(str(self.population.get_fittest().genes[i]))

        self.logger.info("-> genes: " + ''.join(genes))
        self.logger.info("=======================")


    def find_max_fittest_and_add(self):
        # while fittest individuals find the maxium fitness individuals to add to the population
        while self.population.fittest < 5:
            self.generation_count = self.generation_count + 1
            # select 2 fittest individuals
            self.selection()
            # cross them to generate a new individuals
            self.crossover()
            # do mutation under a random probability
            if int(round(random.random() * 100)) %7 < 5:
                self.mutation()

            # then,
            # add fittest offspring to the population
            self.add_offspring()

            # and calculate the fitness value
            self.population.calculate_fitness()

            self.logger.info("-> generation: {}. fittest: {}".format(self.generation_count, self.population.fittest))

    # get 2 fittest individuals from the population
    def selection(self):
        self.fittest = self.population.get_fittest()
        self.second_fittest = self.population.get_second_fittest()

    # 2 fittest individuals "chich"
    def crossover(self):
        # get random crossover point
        crossover_point = random.randrange(self.population.individuals[0].genes_length)
        i = 0
        while i < crossover_point:
            # swap genes between fittest invidiuals
            temp = self.fittest.genes[i]
            self.fittest.genes[i] = self.second_fittest.genes[i]
            self.second_fittest.genes[i] = temp
            i += 1

    def mutation(self):
        # get random mutation point
        mutation_point = random.randrange(self.population.individuals[0].genes_length)

        # flip genes at mutation point
        if self.fittest.genes[mutation_point] == 0:
            self.fittest.genes[mutation_point] = 1
        else:
            self.fittest.genes[mutation_point] = 0

        if self.second_fittest.genes[mutation_point] == 0:
            self.second_fittest.genes[mutation_point] = 1
        else:
            self.second_fittest.genes[mutation_point] = 0

    # get fittest offspring
    def get_fittest_offspring(self):
        if self.fittest.fitness > self.second_fittest.fitness:
            return self.fittest
        return self.second_fittest

    # add fittest offspring
    def add_offspring(self):
        # calculate the fitness 2 top individuals
        self.fittest.calc_fitness()
        self.second_fittest.calc_fitness()

        # remove/replace the least fittest individuals by the fittest offspring
        least_fittest_index = self.population.get_least_fittest_index()

        self.population.individuals[least_fittest_index] = self.get_fittest_offspring()



# test
if __name__ == '__main__':
    logger = Logger()
    genetic = GeneticProcessor(logger)

    genetic.run()

